source: https://ongres.com/blog/explain_analyze_may_be_lying_to_you/

## El efecto Observador
> In physics, the observer effect is the theory that the mere observation of a phenomenon inevitably changes that phenomenon. This is often the result of instruments that, by necessity, alter the state of what they measure in some manner.
> [Observer effect, Wikipedia](https://en.wikipedia.org/wiki/Observer_effect_(physics))

En criollo, lo que el efecto Observador dice es que midiendo una propiedad de un sistema, podrías estar alterando el sistema en sí mismo:  Tu observación se combierte en una versión distorsionada de la realidad.

En muchos casos, esta distorsión es negligible y simplemente podemos ignorarla. Si usamos un termómetro para medir la temperatura de alguien, un poco de calor se va a transferir a de la persona al termómetro, efectivamente disminuyendo la temperatura de la persona. Pero no se debería de notar, y cabría dentro del márgen de error del termómetro.

¿Pero qué pasa cuando la medida no solo afecta, sino que arruina completamente esta medida?


## Donde, potencialmente está la mentira
Seguramente recurras un montón a utilizar el comando de Postgres, [EXPLAIN ANALYZE](https://www.postgresql.org/docs/current/sql-explain.html) cuando quieras optimizar el rendimiento de una consulta. Probablementes mires los nodos de la consulta, veas cuales tienen los mayores tiempos de ejecución y trates de optimizarlos. Cuanto más costoso es un nodo, mayor es el retorno de la inversión que obtendrás si podés optimizarlo. Obviamente, la optimización de una consulta puede variar el plan de ejecución en su totalidad, pero se entiende el punto: querés saber a dónde se está yendo el tiempo de ejecución de la consulta.

En tu Postgres favorito, corré los siguientes comandos:
```create table i1 as select i from generate_series(1,1000*1000) as i;
create table i2 as select i from generate_series(1,1000) as i;
analyze;
explain analyze select sum(i1.i * i2.i) from i1 inner join i2 using (i);
```
---
>  Notar que el primer comando [[analyze]] no está relacionado con el comando [[explain analyze]] que le sigue.
---

Corré la consulta: anotá el tiempo reportado por [[explain analyze]]. Ahora corré la consulta de nuevo y anotá el tiempo de ejecución sin [[explain analyze]]. Podés hacer esto, por ejemplo:
* Corriendo desde psql y usando tanto \\timing y \\o /dev/null
* Usando [pg_stat_statements](https://www.postgresql.org/docs/12/pgstatstatements.html).

El último es el mejor método, ya que el primero incluye el tiempo de round-trip del cliente y el procesamiento. Pero este overhead debería ser negligible para este caso.

¿Ves algo mal? El tiempo de ejecución reportado por [[explain analyze]] es sustancialmente mayor al tiempo real de ejecución de la consulta. En mi sistema, corriendo 20 veces después de otras 20 veces de calentamiento:

|query| 	calls| 	total| 	mean| 	min| 	max| 	stddev|
|------|--------|------|-------|-----|-------|----------|
|explain analyze select sum(i1.i * i2.i) from i1 inner join i2 using (i)| 	20| 	917.20 |	45.86| 	45.32| 	49.24| 	0.84|
|select sum(i1.i * i2.i) from i1 inner join i2 using (i)| 	20| 	615.73| 	30.79| 	30.06| 	34.48| 	0.92|


¡Es cómo un 50% de overhead! Como podemos ver acá, la medida está alterando significativamente el echo observable. Pero puede ponerse mucho peor.  Por ejemplo, en una instancia virtual corriendo en una Instancia EC2 NO-[Nitro](https://aws.amazon.com/ec2/nitro/) (r4.large):

|query| 	calls| 	total| 	mean| 	min| 	max| 	stddev|
|------|--------|------|-------|-----|-------|----------|
|explain analyze select sum(i1.i * i2.i) from i1 inner join i2 using (i)| 	20| 	21080.18|	1054.01 	| 	1053.36| 	1055.96| 	0.55|
|select sum(i1.i * i2.i) from i1 inner join i2 using (i)| 	20| 	2596.85 	| 	129.84| 	129.33| 	130.45 	| 	0.28|

Acá [[explain analyze]] se puso 8 veces más lento, ¡un 700% de overhead!

<Insertar acá imagen de los pinochos sosteniendo el cartel de explain analyze>

Lectores astutos se deben de haber dado cuenta que este efecto tiene que ver con el reloj del sistema. Las instancias NO-Nitro están virtualizadas con Xen, que expone un reloj virtualizado "xen" a las MV. En instancias Nitro y otros entornos virtualizados donde KVM es usado, el reloj es tan rápido como el hipervisor y los resultados son similares como a los primeros mostrados acá. Tambiénd mitigamos este esfuerzo en r4.large al cambiar al time source tsc.

`echo tsc | sudo tee -a /sys/devices/system/clocksource/clocksource0/current_clocksource`

|query| 	calls| 	total| 	mean| 	min| 	max| 	stddev|
|------|-------|------|--------|-----|------|-----------|
|explain analyze select sum(i1.i * i2.i) from i1 inner join i2 using (i)| 	20| 	3747.07| 	187.37| 	187.12| 	187.56| 	0.12|
|select sum(i1.i * i2.i) from i1 inner join i2 using (i)| 	20| 	2602.45| 	130.12| 	129.88| 	130.77| 	0.21|

También notar que los resultados van a cambiar si configurás diferentement `max_parallel_workers_per_gather` ya que estos resultados son afectados por el nivel de paralelismo que se usa.

## Buenas noticias
Sin embargo no deberías estar sorprendido. Este comportamiento es conocido y documentado. Cómo nos tienen acostumbrados, la documentación de Postgres es muy completa:
>The measurement overhead added by EXPLAIN ANALYZE can be significant, especially on machines with slow gettimeofday() operating-system calls. You can use the pg_test_timing tool to measure the overhead of timing on your system.
>[EXPLAIN caveats](https://www.postgresql.org/docs/current/using-explain.html#USING-EXPLAIN-CAVEATS)

Sin embargo, me he dado cuenta que muchos usuarios y Administradores de Baes de Datos o bien desconocen este efecto, o bien no se dan cuenta de que tan significativo puede ser. Este post es mi humilde contribución para que este efecto sea ampliamente entendido.

## El volcán
¿Por qué pasa esto? Postgres, como otras bases de datos OLTP, siguen el modelo de ejecución de consultas llamado [el modelo volcán](https://paperhub.s3.amazonaws.com/dace52a42c07f7f8348b08dc2b186061.pdf). Bajo este modelo, también conocido como "una row a la vez", cada nodo del árbol de ejecución de la consulta contiene código para procesar rows una a una. En vez de que cada nodo obtenga todas las rows correspondientes a si mismo antes de combinar con el siguiente nodo, ni bien una row es obtenido por un nodo, es procesada a través del resto del árbol. Esto tiene sentido -obtener todas las rows de un nodo puede requerir guardar toda la información en memoria, lo cual podría ser imposible-, pero introdujo el problema de [[explain analyze]] descrito acá.
>The executor processes a tree of “plan nodes”. The plan tree is essentially a demand-pull pipeline of tuple processing operations. Each node, when called, will produce the next tuple in its output sequence, or NULL if no more tuples are available. If the node is not a primitive relation-scanning node, it will have child node(s) that it calls in turn to obtain input tuples.
>[src/backend/executor/README](https://github.com/postgres/postgres/blob/master/src/backend/executor/README#L6)


Entonces, ya explicamos de donde viene este overhead: En órden de medir el tiempo de ejecución de un nodo dado, como es mostrado por [[explain analyze]], debés medir el tiempo de ejecución en base  a cada row, y entonces juntarlas por nodo, para obener el tiempo total de ejecución por nodo.

Ya que las rows no son ejecutadas una después de otra (ya que una row puede ser procesada por otros nodos primero), basicamente necesitás obtener la hora del sistema antes y después de procesar cada row. En otras palabras: estás haciendo una llamada al sistema dos veces por row.  En un nodo que procesa millones de rows, estás llamando al sistema millones de veces.

¿Pero que tan barato (o caro) es llamar al reloj del sistema? En Postgres, esto está implementado en la función [elapsed_time](https://github.com/postgres/postgres/blob/d9101e9806e446a413bcef16d3ab65602ed028ad/src/backend/commands/explain.c#L1028), que a su vez se basa en el macro [[INSTR_TIME]] definido en [instr_time.h](https://github.com/postgres/postgres/blob/7559d8ebfa11d98728e816f6b655582ce41150f3/src/include/portability/instr_time.h). Que hace la llamada al sistema [[clock_gettime]], una llamada al sistema rápida en muchos sistemas. En particular, en Linux, es tipicamente implementada como [VDSO](https://en.wikipedia.org/wiki/VDSO), significando que no hay un cambio de contexto entre el espacio del usuario y del kernel, haciendo que la llamada sea significativamente rápida.

Pero de nuevo, ¿qué tan rápido es 'rápido', si podemos hacer esta llamada millones de veces? De nuevo, la [documentación de Postgres](https://www.postgresql.org/docs/12/pgtesttiming.html) al rescate, ya que hay un binario incluido en Postgres para hacer precisamente esto, [[pg_test_timing]]. De hecho, tiene una [[sección en la documentación]](https://www.postgresql.org/docs/12/pgtesttiming.html#id-1.9.5.11.7.3) explicando como usarlo para medir el overhead de [[explain analyze]].

En uno de los sistemas usados para las medidas anteriores, reporta:
```Testing timing overhead for 3 seconds.
Per loop time including overhead: 4620,82 ns
Histogram of timing durations:
  < us   % of total      count
     1      0,00000          0
     2      0,00000          0
     4      0,00000          0
     8     99,85491     648295
    16      0,01586        103
    32      0,12060        783
    64      0,00863         56
```
Básicamente, el overhead es para la mayoría de los casos alrededor de 5 microsegundos. Este tiempo multiplicado por millones significa segundos o docenas de segundos de overhead. Recomiendo que leas [Fuentes de reloj en Linux](http://btorpey.github.io/blog/2014/02/18/clock-sources-in-linux/) si querés profundizar en el tema.

## No tan buenas noticias
Volvamos a nuestro objetivo de usar la información del tiempo de ejecución para ver si podemos optimizar una consulta. Si verificar el overhead es sustancial, pero es proporcional al tiempo real de ejecución, no debería importar mucho- ya que todos los tiempos de ejecución de las consultas deberían escalar a su vez, y el nodo más lento seguiría siendo el nodo más lento. Pero el problema es que no hay nodos que sufren significativamente de demasiado overhead, y parecen ser más lentos que los otros, mientras que este no es el caso real. Desafortunadamente esto significa que no podés confiar en [[explain analyze]] para optimizar tus consultas.

Depende completamente de la consulta y sus nodos de ejecución. Podemos usar [[ltrace]] para contar el número de veces que [[clock_gettime]] es usado:
`sudo ltrace -p $postgres_backend_pid -c -e clock_gettime`

|query| 	clock_gettime calls| 	parallel|
|------|----------------------|--------|
|explain analyze select sum(i1.i * i2.i) from i1 inner join i2 using (i)| 	2004028| 	off|
|select sum(i1.i * i2.i) from i1 inner join i2 using (i)| 	4| 	off|
|explain analyze select sum(i2.i) from i2| 	2016| 	off|
|explain analyze select sum(i1.i * i2.i) from i1 inner join i2 using (i)| 	38656| 	on|

Acá hay algunos ejemplos. Para la consulta con el join, observamos que el reloj es llamado 2N+4M+K veces, donde N es el número de rows de i1, M es el número de rows de i2 y K es el factor constante; 28 en este caso, 16 en el caso de la consulta que suma (La tercera).

Es muy interesante el caso cuando el modo paralelo es activado. Acá el tiempo es reportado en bloques que en cierto grado bajando el número en que el reloj es llamado.

El overhead que [[explain analyze]] introduce, no es proporcional a la duración real del plan de ejecución, sino casi proporcional al número de rows procesadas por el nodo. Mientras que están alineados, más rows procesadas no siempre lleva a mayores tiempos de ejecución, y contar con esta suposición puede llevar a creer que un nodo es más lento cuando en realidad es más rápido que otro. Y por lo tanto llevando a una mala estrategia de optimización de la consulta.

He trabajando profundamente en este tema e intenté lo siguiente:
* Toma algunas consultas y el número de llamadas a [[clock_gettime]], como en la anterior tabla, y medí el tiempo de ejecución de [[explain analyze]](sin el overhead adicional introducido por ltrace). Después resolvé la ecuación con el tiempo de reloj como incógnita. Sin emargo, el resultado varían significativamente de ejecución a ejecución, y no son comparables. Obtuve resultados que divergen hasta una orden de magnitud. Ni si quiera una regresión ayuda con resultados tan dispares.
* Intentá medir el overhead de [[clock_gettime]] con el más rápido y más avanzado profiler de rendimiento: [eBPF](http://www.brendangregg.com/blog/2019-01-01/learn-ebpf-tracing.html). Sin embargo, incluso el overhead de BPF es mayor que [[clock_gettime]], dejándolo sin efecto.

## Como ser un poco más confiable
Supongo que no es fácil. El hecho de que el modo paralelo parece llamar al tiempo del sistema en bloque, en algunos casos, puede no ser una buena manera de seguir adelante.
Otra alternativa podría ser proveer de un "mecanismo de corrección". Si el tiempo del reloj puede ser medido precisamente, y el número de veces que el reloj es llamado, es sabido -Postgres ciertamente podría mantener el registro de esto-, su contribución podría ser restada del tiempo total medido. Aquneu probablemente no 100% exacto, podría ser mucho mejor de lo que es hoy.

## Pensamientos extra
En realidad, [[explain analyze]] es un profiler de ejecución de consultas. Teniendo en cuenta esto, todos sabemos que los profiles introducen más o menos overhead. Esta es la clave para quedarse: [[explain analyze]] es un profiler, y su overhead va del rango de alto a muy/extramademente alto en sistemas con relojes lentos virtualizados.

---
¿Por qué [[explain]] and [[explain analyze]] comparten el mismo "comando"? Son, en realidad, dos cosas muy distintas: El primero te da el plan de ejecución de la consulta; el segundo hace un profile d ela consulta. Mientras que el resultado es similar -solamente eso, similar- son dos cosas muy distintas. Yo renombraría el segundo a PROFILE SELECT...

Más o menos la misma historia con VACUUM y VACUUM FULL. El último debería ser renombrado a DEFRAG o REWRITE TABLE -o VACUUM FULL SI REALMENTE SE LO QUE ESTOY HACIENDO POR FAVOR BLOQUEA MI TABLA.